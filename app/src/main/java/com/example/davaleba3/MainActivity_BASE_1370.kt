package com.example.davaleba3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var isFirstPlayer: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener() {
            checkPlayer(button00)
        }

        button01.setOnClickListener() {
            checkPlayer(button01)
        }
        button02.setOnClickListener() {
            checkPlayer(button02)
        }
        button10.setOnClickListener() {
            checkPlayer(button10)
        }
        button11.setOnClickListener() {
            checkPlayer(button11)
        }
        button12.setOnClickListener() {
            checkPlayer(button12)
        }
        button20.setOnClickListener() {
            checkPlayer(button20)
        }
        button21.setOnClickListener() {
            checkPlayer(button21)
        }
        button22.setOnClickListener() {
            checkPlayer(button22)
        }


    }

    private fun checkPlayer(button: Button) {
        if (button.text.isEmpty()) {
            if (isFirstPlayer) {
                button.text = "X"
                isFirstPlayer = false
            } else {
                button.text = "O"
                isFirstPlayer = true
            }
        }

        if (button00.text.isNotEmpty() && button00.text == button01.text && button00.text == button02.text) {
            Toast.makeText(this, "winner is${button00.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button10.text.isNotEmpty() && button10.text == button11.text && button11.text == button12.text) {
            Toast.makeText(this, "winner is${button10.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button20.text.isNotEmpty() && button20.text == button21.text && button21.text == button22.text) {
            Toast.makeText(this, "winner is${button20.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button00.text.isNotEmpty() && button00.text == button10.text && button10.text == button20.text) {
            Toast.makeText(this, "winner is${button00.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button01.text.isNotEmpty() && button01.text == button11.text && button11.text == button21.text) {
            Toast.makeText(this, "winner is${button01.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button02.text.isNotEmpty() && button02.text == button12.text && button12.text == button22.text) {
            Toast.makeText(this, "winner is${button02.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button20.text.isNotEmpty() && button20.text == button11.text && button11.text == button02.text) {
            Toast.makeText(this, "winner is${button20.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
        else if (button00.text.isNotEmpty() && button00.text == button11.text && button10.text == button22.text) {
            Toast.makeText(this, "winner is${button00.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
        }
    }

    private fun disableClicks(){
        button00.isEnabled = false
        button01.isEnabled = false
        button02.isEnabled = false
        button10.isEnabled = false
        button11.isEnabled = false
        button12.isEnabled = false
        button20.isEnabled = false
        button21.isEnabled = false
        button22.isEnabled = false
    }


}
