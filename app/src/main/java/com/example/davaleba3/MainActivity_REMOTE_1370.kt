package com.example.davaleba3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var isFirstPlayer: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener() {
            checkPlayer(button00)
        }

        button01.setOnClickListener() {
            checkPlayer(button01)
        }
        button02.setOnClickListener() {
            checkPlayer(button02)
        }
        button10.setOnClickListener() {
            checkPlayer(button10)
        }
        button11.setOnClickListener() {
            checkPlayer(button11)
        }
        button12.setOnClickListener() {
            checkPlayer(button12)
        }
        button20.setOnClickListener() {
            checkPlayer(button20)
        }
        button21.setOnClickListener() {
            checkPlayer(button21)
        }
        button22.setOnClickListener() {
            checkPlayer(button22)
        }

    }

    private fun checkPlayer(button: Button) {
        if (button.text.isEmpty()) {
            if (isFirstPlayer) {
                button.text = "X"
                isFirstPlayer = false
            } else {
                button.text = "O"
                isFirstPlayer = true
            }
        }

        if (check(button00,button01,button02)) {
            popUp("Winenr is '${button01.text}'")
        }
        else if (check(button10,button11,button12)) {
            popUp("Winenr is '${button11.text}'")
        }
        else if (check(button20,button21,button22)) {
            popUp("Winenr is '${button21.text}'")
        }
        else if (check(button00,button10,button20)) {
            popUp("Winenr is '${button10.text}'")
        }
        else if (check(button01,button11,button21)) {
            popUp("Winenr is '${button11.text}'")
        }
        else if (check(button02,button12,button22)) {
            popUp("Winenr is '${button12.text}'")
        }
        else if (check(button00,button11,button22)) {
            popUp("Winenr is '${button11.text}'")
        }
        else if (check(button12,button11,button20)) {
            popUp("Winenr is '${button11.text}'")
        }
        else
        {
            popUp("Have no winner")
        }
    }

    private fun check(button: Button,button1: Button,button2: Button):Boolean
    {
        return button.text.isNotEmpty() && button1.text.isNotEmpty() && button2.text.isNotEmpty() && button.text == button1.text && button1.text == button2.text
    }

    private fun popUp(text: String)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        disableClicks()
        //reset()
    }

    private fun disableClicks(){
        button00.isEnabled = false
        button01.isEnabled = false
        button02.isEnabled = false
        button10.isEnabled = false
        button11.isEnabled = false
        button12.isEnabled = false
        button20.isEnabled = false
        button21.isEnabled = false
        button22.isEnabled = false
    }

    private fun reset()
    {
        //Delay(200)


        button00.text = ""
        button01.text = ""
        button02.text = ""
        button10.text = ""
        button11.text = ""
        button12.text = ""
        button20.text = ""
        button21.text = ""
        button22.text = ""

        button00.isEnabled = true
        button01.isEnabled = true
        button02.isEnabled = true
        button10.isEnabled = true
        button11.isEnabled = true
        button12.isEnabled = true
        button20.isEnabled = true
        button21.isEnabled = true
        button22.isEnabled = true
    }


}
