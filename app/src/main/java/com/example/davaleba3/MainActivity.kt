package com.example.davaleba3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var isFirstPlayer: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener() {
            checkPlayer(button00)
        }

        button01.setOnClickListener() {
            checkPlayer(button01)
        }
        button02.setOnClickListener() {
            checkPlayer(button02)
        }
        button10.setOnClickListener() {
            checkPlayer(button10)
        }
        button11.setOnClickListener() {
            checkPlayer(button11)
        }
        button12.setOnClickListener() {
            checkPlayer(button12)
        }
        button20.setOnClickListener() {
            checkPlayer(button20)
        }
        button21.setOnClickListener() {
            checkPlayer(button21)
        }
        button22.setOnClickListener() {
            checkPlayer(button22)
        }
        resetbut.setOnClickListener(){
            reset()
        }

    }

    private fun checkPlayer(button: Button) {
        if (button.text.isEmpty()) {
            if (isFirstPlayer) {
                button.text = "X"
                isFirstPlayer = false
            } else {
                button.text = "O"
                isFirstPlayer = true
            }
        }

        if (check(button00,button01,button02)) {
            popUp("Winenr is '${button01.text}'")
        }
        else if (check(button10,button11,button12)) {
            popUp("Winenr is '${button11.text}'")
        }
        else if (check(button20,button21,button22)) {
            popUp("Winenr is '${button21.text}'")
        }
        else if (check(button00,button10,button20)) {
            popUp("Winenr is '${button10.text}'")
        }
        else if (check(button01,button11,button21)) {
            popUp("Winenr is '${button11.text}'")
        }
        else if (check(button02,button12,button22)) {
            popUp("Winenr is '${button12.text}'")
        }
        else if (check(button00,button11,button22)) {
            popUp("Winenr is '${button11.text}'")
        }
        else if (check(button12,button11,button20)) {
            popUp("Winenr is '${button11.text}'")
        }
<<<<<<< HEAD
        else if (button00.text.isNotEmpty() && button00.text == button11.text && button00.text == button22.text) {
            Toast.makeText(this, "winner is${button00.text}", Toast.LENGTH_SHORT).show()
            disableClicks()
=======
        else
        {
            popUp("Have no winner")
>>>>>>> 2f1916bfae30b37ab0da8557cddee32f54a7a9d9
        }
        else if(button00.text.isNotEmpty() && button01.text.isNotEmpty() && button02.text.isNotEmpty() && button10.text.isNotEmpty()&& button11.text.isNotEmpty() && button12.text.isNotEmpty() && button20.text.isNotEmpty() && button21.text.isNotEmpty() && button22.text.isNotEmpty()){
            Toast.makeText(this, "draw", Toast.LENGTH_SHORT).show()
        }
    }

    private fun check(button: Button,button1: Button,button2: Button):Boolean
    {
        return button.text.isNotEmpty() && button1.text.isNotEmpty() && button2.text.isNotEmpty() && button.text == button1.text && button1.text == button2.text
    }

    private fun popUp(text: String)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        disableClicks()
        //reset()
    }

    private fun disableClicks(){
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false
    }
    private fun reset(){
        button00.isClickable = true
        button01.isClickable = true
        button02.isClickable = true
        button10.isClickable = true
        button11.isClickable = true
        button12.isClickable = true
        button20.isClickable = true
        button21.isClickable = true
        button22.isClickable = true

<<<<<<< HEAD
=======
    private fun reset()
    {
        //Delay(200)


>>>>>>> 2f1916bfae30b37ab0da8557cddee32f54a7a9d9
        button00.text = ""
        button01.text = ""
        button02.text = ""
        button10.text = ""
        button11.text = ""
        button12.text = ""
        button20.text = ""
        button21.text = ""
        button22.text = ""

<<<<<<< HEAD
    }
=======
        button00.isEnabled = true
        button01.isEnabled = true
        button02.isEnabled = true
        button10.isEnabled = true
        button11.isEnabled = true
        button12.isEnabled = true
        button20.isEnabled = true
        button21.isEnabled = true
        button22.isEnabled = true
    }

>>>>>>> 2f1916bfae30b37ab0da8557cddee32f54a7a9d9

}
